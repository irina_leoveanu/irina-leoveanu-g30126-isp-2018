package g30126.leoveanu.irina.l9.ex2;

import java.io.*;


public class CounterForChars implements Serializable {
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader("input.txt"));
        String s, s2 = new String();
        int counter=0;
        while ((s = in.readLine()) != null)
            for(int i = 0; i<s.length();i++)
                if(s.charAt(i)=='a') counter++;
        System.out.println(counter);
        in.close();

    }
}