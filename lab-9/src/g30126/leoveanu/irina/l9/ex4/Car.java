package g30126.leoveanu.irina.l9.ex4;

import java.io.Serializable;

public class Car implements Serializable {
    private String model;
    private double price;
    transient int id;

    public Car(String model, double price) {
        this.model = model;
        this.price = price;
        id =  (int)(Math.random()*100);
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                ", id=" + id +
                '}';
    }
}
