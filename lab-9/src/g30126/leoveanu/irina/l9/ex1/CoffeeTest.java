package g30126.leoveanu.irina.l9.ex1;

public class CoffeeTest {
    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for(int i = 0;i<15;i++){
            try {
                Coffee c=mk.makeCoffee();
                    d.drinkCoffee(c);
                } catch (TemperatureException e) {
                    System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
                } catch (ConcentrationException e) {
                    System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
                }  catch (TooManyCoffeesException e) {
                System.out.println("Exception:"+e.getMessage()+" coffees="+e.getCoffes());
            }finally {
                    System.out.println("Throw the coffee cup.\n");
                }
            }
        }
    }

class CoffeeMaker {
    private static final int MAX_COFFEES = 3;
    private int count=0;
    Coffee makeCoffee() throws  TooManyCoffeesException {
        if (count > MAX_COFFEES) {
            throw new TooManyCoffeesException(count, "Too many coffees");
        } else {
            System.out.println("Make a coffee");
            int t = (int) (Math.random() * 100);
            int c = (int) (Math.random() * 100);
            Coffee coffee = new Coffee(t, c);
            count++;
            return coffee;
        }
    }
}//.class

class Coffee{
    private int temp;
    private int conc;

    Coffee(int t,int c){temp = t;conc = c;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    public String toString(){return "[coffee temperature="+temp+":concentration="+conc+"]";}
}//.class

class CoffeeDrinker{
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Coffee is too hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Coffee concentration too high!");
        System.out.println("Drink coffee:"+c);
    }
}//.class

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
        super(msg);
        this.t = t;
    }
    int getTemp(){
        return t;
    }
}//.class

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c,String msg) {
        super(msg);
        this.c = c;
    }
    int getConc(){
        return c;
    }
}//.class

class TooManyCoffeesException extends Exception{
    int c;
    public TooManyCoffeesException(int c,String msg) {
        super(msg);
        this.c = c;
    }
    int getCoffes(){
        return c;
    }
}//.class