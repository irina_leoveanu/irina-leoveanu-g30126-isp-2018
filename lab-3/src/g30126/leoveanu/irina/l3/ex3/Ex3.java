package g30126.leoveanu.irina.l3.ex3;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class Ex3 {
	public static void main(String[] args)
	   {
	      // Set up the initial situation
	      City LA = new City(); 
	      Robot iribotel = new Robot(LA, 6,1, Direction.NORTH); 
	      
	      // iribotel moves 5 times north
	      iribotel.move();
	      iribotel.move();
	      iribotel.move();
	      iribotel.move();
	      iribotel.move();
	      iribotel.turnLeft();     // start turning around by turning 2 times turning left
	      iribotel.turnLeft();     // finished turning around
	      iribotel.move();
	      iribotel.move();
	      iribotel.move();
	      iribotel.move();
	      iribotel.move(); // arrieves at starting point
}
}
