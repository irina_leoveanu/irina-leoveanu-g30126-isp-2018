package g30126.leoveanu.irina.l3.ex6;

public class TestMyPoint {

	public static void main(String[] args)
	{
		MyPoint A = new MyPoint();  // Test constructor
		System.out.println(A.toString());      // Test toString()
		A.setX(2);   // Test setters
		A.setY(4);
		System.out.println("x is: " + A.getX());  // Test getters
		System.out.println("y is: " + A.getY());
		A.setXY(8, 2);   // Test setXY()
		System.out.println(A);
		MyPoint B = new MyPoint(16, 4);  // Test the  another constructor
		System.out.println(B);
		// Testing the methods distance()
		System.out.println(A.distance(B));    
		System.out.println(B.distance(A));    
		System.out.println(A.distance(12, 4));  
	}
}
