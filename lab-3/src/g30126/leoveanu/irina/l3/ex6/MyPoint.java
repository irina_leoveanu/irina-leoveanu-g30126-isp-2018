package g30126.leoveanu.irina.l3.ex6;

public class MyPoint {
private int x,y;
//no-argument Constructor
 	public MyPoint()
 	{
 		x=0;
 		y=0;
 	}
 	//constructs a point with the given x and y coordinates.
 	public MyPoint( int a, int b )
 	{
 		x=a;
 		y=b;
 	}   
 	public int getX()
 	{
 		return x;
 	}
 	public void setX(int x)
 	{
 		this.x=x;
 	}
 	public int getY()
 	{
 		return y;
 	}
 	public void setY(int y)
 	{
 		this.y=y;
 	}

 	public void setXY(int x, int y)
 	{
 		this.x=x;
 		this.y=y;
 	}
 	public String toString()
 	{
 		return "("+x+","+y+")";
 	}
 	public double distance(int x, int y) 
 	{
 		int dx=this.x-x;
 		int dy=this.y-y;
 		return Math.sqrt(dx*dx+dy*dy);
 	}
 	public double distance(MyPoint another) 
 	{

 		int dx=another.x-this.x;
 		int dy=another.y-this.y;
 		return Math.sqrt(dx*dx+dy*dy);
 	}
 	 	
}
