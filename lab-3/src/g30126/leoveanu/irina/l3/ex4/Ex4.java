package g30126.leoveanu.irina.l3.ex4;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class Ex4 {
	public static void main(String[] args)
	   {
	      // Set up the initial situation
	      City LA = new City(); //creeaza oras
	      Wall blockAve0 = new Wall(LA, 1, 1, Direction.WEST);
	      Wall blockAve1 = new Wall(LA, 2, 1, Direction.WEST);
	      Wall blockAve2 = new Wall(LA, 1, 1, Direction.NORTH);
	      Wall blockAve3 = new Wall(LA, 1, 2, Direction.NORTH);
	      Wall blockAve4 = new Wall(LA, 1, 2, Direction.EAST);
	      Wall blockAve5 = new Wall(LA, 2, 2, Direction.EAST);
	      Wall blockAve6 = new Wall(LA, 2, 1, Direction.SOUTH);
	      Wall blockAve7 = new Wall(LA, 2, 2, Direction.SOUTH);
	      Robot iribotel = new Robot(LA, 0, 2, Direction.WEST); 
	      iribotel.move();
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.move();
	      iribotel.move();
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.move();
	      iribotel.move();
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.move();
	      iribotel.move();
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.move();
	   }
}
