package g30126.leoveanu.irina.l11.ex3;

public class Counter extends Thread {

    Counter(String name){
        super(name);
    }

    public void start(int n1, int n2){
        for(int i=n1;i<n2;i++){
            System.out.println(getName() + " i = "+i);
            try {
                Thread.sleep((int)(Math.random() * 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }

    public static void main(String[] args) {
        Counter c1 = new Counter("counter1");
        Counter c2 = new Counter("counter2");

        c1.start(0,100);
        c2.start(100,200);

    }
}