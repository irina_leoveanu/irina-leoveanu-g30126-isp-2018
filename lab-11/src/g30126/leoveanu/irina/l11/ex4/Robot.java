package g30126.leoveanu.irina.l11.ex4;

import java.util.Random;

public class Robot extends  Thread{
    private int x,y;
Robot(String name,int x, int y)
{
    super(name);
    this.x = x;
    this.y = y;
}
    public void moveLeft(){
    if(this.x>0) setX(x-1);
    }
    public void moveRight(){
        if(this.x<100) setX(x+1);
    }
    public void moveUp(){
        if(this.y>0) setY(y-1);
    }
    public void moveDown(){
        if(this.y<100) setY(y+1);
    }
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public void run(){
    Random r = new Random();
    while(true)
    {           int k = r.nextInt(4);
            if(k==0) {moveDown(); System.out.println(getName()  + " moved  down  i = "+x+" j = "+y);}
            if(k==1) {moveUp();   System.out.println(getName()  + " moved  up    i = "+x+" j = "+y);}
            if(k==2) {moveLeft(); System.out.println(getName()  + " moved  left  i = "+x+" j = "+y);}
            if(k==3) {moveRight();System.out.println(getName()  + " moved  right i = "+x+" j = "+y);}
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //System.out.println(getName() + " job finalised.");
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public static void main(String[] args) {
        Robot r = new Robot("robotel", 5,5);
        r.start();
    }
}