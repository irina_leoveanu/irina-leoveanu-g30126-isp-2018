package g30126.leoveanu.irina.l11.ex2;
public class TestSincronizareInitial {
    public static void main(String[] args) {
        PunctIN p = new PunctIN();
        FirSetIN fs1 = new FirSetIN(p);
        FirGetIN fg1 = new FirGetIN(p);

        fs1.start();
        fg1.start();
    }
}

class FirGetIN extends Thread {
    PunctIN p;

    public FirGetIN(PunctIN p){
        this.p = p;
    }

    public void run(){
        int i=0;
        int a,b;
        while(++i<15){
            // synchronized(p){
            a= p.getX();
            try {
                sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            b = p.getY();
            // }
            System.out.println("Am citit: ["+a+","+b+"]");
        }
    }
}//.class


class FirSetIN extends Thread {
    PunctIN p;
    public FirSetIN(PunctIN p){
        this.p = p;
    }
    public void run(){
        int i =0;
        while(++i<15){
            int a = (int)Math.round(10*Math.random()+10);
            int b = (int)Math.round(10*Math.random()+10);

            //synchronized(p){
            p.setXY(a,b);
            // }

            try {
                sleep(10);
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
            System.out.println("Am scris: ["+a+","+b+"]");
        }
    }
}//.class

class PunctIN {
    int x,y;
    public void setXY(int a,int b){
        x = a;y = b;
    }
    public int getX(){return x;}
    public int getY(){return y;}
}