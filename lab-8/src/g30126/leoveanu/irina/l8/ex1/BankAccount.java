package g30126.leoveanu.irina.l8.ex1;

import java.util.Objects;

public class BankAccount implements Comparable {
    private String owner;
    private double balance;

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }
    public int compareTo(Object o) {
        BankAccount BA = (BankAccount) o;
        if(balance>BA.getBalance()) return 1;
        if(balance==BA.getBalance()) return 0;
        return -1;
    }
    public void withdraw(double amount){
        if(this.balance >= amount)
            this.balance -= amount;
        else System.out.println("Balance too low for "+this.owner);
    }

    public void deposit(double amount){
        this.balance+=amount;
        System.out.println("Balance updated for "+this.owner);
    }

    @Override
    public boolean equals(Object o) {
        boolean result;
        if (this == o) result = true;
        if (o == null || getClass() != o.getClass()) result = false;
        BankAccount that = (BankAccount) o;
        result = Double.compare(that.balance, balance) == 0 &&
                Objects.equals(owner, that.owner);
        return result;
    }

    @Override
    public int hashCode() {
//        int result = 19;
//        result = 37 * result + owner.hashCode();
//        result = 37 * result + (int)balance;
//        return result;
    return Objects.hash(owner, balance);
    }

    public static void main(String[]args)
    {
        BankAccount b1 = new BankAccount("Popescu Ion",500);
        BankAccount b2 = new BankAccount("Popescu Maria",250);
        BankAccount b3 = new BankAccount("Ionescu Maria",1000);
        BankAccount b4 = b3;
        System.out.println(b4.equals(b3));
        System.out.println(b4.equals(b2));
        System.out.println(b1.hashCode());
        System.out.println("Check if withdraw and deposit work");
        b2.deposit(300);
        b2.withdraw(600);
        b3.deposit(600);

    }
}
