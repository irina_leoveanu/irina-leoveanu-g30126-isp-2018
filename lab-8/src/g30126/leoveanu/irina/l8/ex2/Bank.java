package g30126.leoveanu.irina.l8.ex2;

import g30126.leoveanu.irina.l8.ex1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
public class Bank {
    ArrayList <BankAccount> accounts = new ArrayList<>();
    public void addAccount(String owner, double balance)
    {
        BankAccount b1 = new BankAccount(owner,balance);
        accounts.add(b1);
    }

    public void printAccounts()
    {
        Collections.sort(accounts);
        for(int i=0;i<accounts.size();i++){
            BankAccount ba = (BankAccount)accounts.get(i);
            System.out.println(ba.getOwner()+" "+ba.getBalance());
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (int i = 0; i < accounts.size(); i++) {
            BankAccount ba = (BankAccount) accounts.get(i);
            if (ba.getBalance() >= minBalance && ba.getBalance() <= maxBalance)
                System.out.println(ba.getOwner() + " " + ba.getBalance());
        }
    }
    public BankAccount getAccount(String owner)
    {
        for(BankAccount o:accounts){
            if(owner.equals(o.getOwner()))
                return o;
        }
        return null;
    }

    public ArrayList<BankAccount> getAllAccounts() {
        return accounts;
    }

    public static void main(String[] args)
    {
        Bank bank = new Bank();
        bank.addAccount("Popescu Ion",500);
        bank.addAccount("Popescu Maria",250);
        bank.addAccount("Ionescu Maria",1000);
        bank.printAccounts();
        bank.printAccounts(300,1000);
        BankAccount B1 = bank.getAccount("Popescu Ion");
        System.out.println(B1.getOwner()+" "+B1.getBalance());
        ArrayList <BankAccount> al = bank.getAllAccounts();
//        for (int i = 0; i<al.size();i++)
//            for (int j= i+1; j<al.size();j++) {
//                BankAccount ba1 = (BankAccount) al.get(j);
//                BankAccount ba2 = (BankAccount) al.get(i);
//                if(ba2.getOwner() > ba1.getOwner())
//                {BankAccount x = ba1;
//                ba1=ba2;
//                ba2=x;}
//
//            }

    }
}
