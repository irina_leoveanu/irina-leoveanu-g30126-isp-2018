package g30126.leoveanu.irina.l8.ex3;

import g30126.leoveanu.irina.l8.ex1.BankAccount;

import java.util.Comparator;
import java.util.TreeSet;

public class Bank {
    TreeSet <BankAccount> accounts = new TreeSet<>();
    public void addAccount(String owner, double balance)
    {
        BankAccount b1 = new BankAccount(owner,balance);
        accounts.add(b1);
    }

    public void printAccounts()
    {
        for(BankAccount ba:accounts){
            System.out.println(ba);
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for(BankAccount ba:accounts){
            if(ba.getBalance()>minBalance && ba.getBalance()<maxBalance)
            System.out.println(ba);
        }
    }
    public BankAccount getAccount(String owner)
    {
        for(BankAccount o:accounts){
            if(owner.equals(o.getOwner()))
                return o;
        }
        return null;
    }

    public TreeSet<BankAccount> getAllAccounts() {
        return accounts;
    }

    public static void main(String[] args)
    {
        Bank bank = new Bank();
        bank.addAccount("Popescu Ion",500);
        bank.addAccount("Popescu Maria",250);
        bank.addAccount("Ionescu Maria",1000);
        bank.printAccounts();
        bank.printAccounts(300,1000);
        BankAccount B1 = bank.getAccount("Popescu Ion");
        System.out.println(B1.getOwner()+" "+B1.getBalance());
        TreeSet <BankAccount> al = new TreeSet<BankAccount>(new MyOwnerComp());
        al.addAll(bank.getAllAccounts());

    }
}
class MyOwnerComp implements Comparator<BankAccount> {

    @Override
    public int compare(BankAccount e1, BankAccount e2) {
        return e1.getOwner().compareTo(e2.getOwner());
    }
}
class MybalanceComp implements Comparator<BankAccount> {

    @Override
    public int compare(BankAccount e1, BankAccount e2) {
        if(e1.getBalance() > e2.getBalance()){
            return 1;
        } else {
            return -1;
        }
    }
}
