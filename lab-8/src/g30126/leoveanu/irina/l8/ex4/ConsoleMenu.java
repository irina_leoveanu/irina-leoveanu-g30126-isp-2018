package g30126.leoveanu.irina.l8.ex4;
//import jexer.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConsoleMenu extends JFrame {
    Dictionary dictionary = new Dictionary();
    JLabel word, def;
    JTextField tWord,tDef;
    JTextArea tArea;
    JButton bAdd, bSearch,bShow, bShowAll;

    ConsoleMenu()
    {
        dictionary.addWord(new Word("i"),new Definition("the person speaking"));
        dictionary.addWord(new Word("you"),new Definition("the person speaking to "));
        dictionary.addWord(new Word("he"),new Definition("the person talking about"));
        setTitle("Test dictionary");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500,600);
        setVisible(true);
    }
    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        word = new JLabel("Word ");
        word.setBounds(50,50, width, height);

        def = new JLabel("Definition ");
        def.setBounds(50, 80,width, height);

        tWord = new JTextField();
        tWord.setBounds(110,50,width, height);

        tDef = new JTextField();
        tDef.setBounds(110,80,width, height);

        bSearch = new JButton("Search");
        bSearch.setBounds(50,150,width, height);

        bShow = new JButton("Show");
        bShow.setBounds(50,180,width, height);

        bShowAll = new JButton("Show All");
        bShowAll.setBounds(50,210,width, height);

        bAdd = new JButton("Add");
        bAdd.setBounds(50,240,width, height);

        bSearch.addActionListener(new TratareButonSearch());

        bShow.addActionListener(new TratareButonShow());

        bShowAll.addActionListener(new TratareButonShowAll());

        bAdd.addActionListener(new TratareButonAdd());

        tArea = new JTextArea();
        tArea.setBounds(10,250,450,300);

        add(word);add(def);add(tWord);add(tDef);add(bSearch);add(bShow);add(bShowAll);
        add(tArea);

    }
    public static void main(String[] args) {
        new ConsoleMenu();
    }
    class TratareButonSearch implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            Word word = new Word(ConsoleMenu.this.tWord.getText());
            Definition def;

            if(ConsoleMenu.this.dictionary.getDefinition(word).equals(null)) {
                //word not found
                ConsoleMenu.this.tArea.append("Word not found\n");
            }
                else{
                def = ConsoleMenu.this.dictionary.getDefinition(word);
                ConsoleMenu.this.tArea.append(word.getWord() +" = "+def.getDefinition()+"\n");

                }
            }
        }

    class TratareButonShow implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ConsoleMenu.this.tArea.append(dictionary.getAllWords()+"\n");
        }
    }

    class TratareButonShowAll implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            ConsoleMenu.this.tArea.append(dictionary.getAllDefinitions()+"\n");
        }
    }
    class TratareButonAdd implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            Word word = new Word(ConsoleMenu.this.tWord.getText());
            Definition def = new Definition(ConsoleMenu.this.tDef.getText());

            if(ConsoleMenu.this.dictionary.getDefinition(word).equals(null)) {
                //word not found
                dictionary.addWord(word,def);
                ConsoleMenu.this.tArea.append("Word added\n");
            }
            else{
                //word found
                ConsoleMenu.this.tArea.append("Word already in the dictionary\n");
            }
        }
    }
    }


