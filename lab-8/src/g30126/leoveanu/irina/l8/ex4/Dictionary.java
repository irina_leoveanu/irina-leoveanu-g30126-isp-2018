package g30126.leoveanu.irina.l8.ex4;

import java.util.*;

public class Dictionary {
    Map<Word, Definition> map = new HashMap<Word, Definition>();

    public void addWord(Word w, Definition d) {
        map.put(w, d);
    }

    public Definition getDefinition(Word w) {
        Definition def = null;
        return map.get(w);
    }

    public ArrayList<Word> getAllWords() {
//        for(Map.Entry<Word,Definition> entry:map.entrySet()) {
//            Word word = entry.getKey();
//            System.out.println(word.getWord());
//        }
        ArrayList<Word> all = new ArrayList<>();
        for(Map.Entry<Word,Definition> entry:map.entrySet()) {
            Word word = entry.getKey();
            all.add(word);
        }
            return all;
    }

    public Map<Word, Definition> getAllDefinitions() {
//       for(Map.Entry<Word,Definition> entry:map.entrySet()) {
//           Word word = entry.getKey();
//           Definition definition = entry.getValue();
//           System.out.println(word.getWord()+ " = " + definition.getDefinition());
//       }
        return map;
    }
}