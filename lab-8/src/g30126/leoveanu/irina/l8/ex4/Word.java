package g30126.leoveanu.irina.l8.ex4;


public class Word {
    private String name;

    public Word(String name) {
        this.name = name;
    }

    public String getWord() {
        return name;
    }
}
