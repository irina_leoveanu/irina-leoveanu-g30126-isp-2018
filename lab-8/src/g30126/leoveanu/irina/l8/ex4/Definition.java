package g30126.leoveanu.irina.l8.ex4;


public class Definition {
    private String description;

    public Definition(String description) {
        this.description = description;
    }

    public String getDefinition() {
        return description;
    }
}
