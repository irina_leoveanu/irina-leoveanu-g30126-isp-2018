package g30126.leoveanu.irina.l6.ex1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color, int length, String id, int x, int y, boolean fill) {
        super(color,id,x,y,fill);
        this.length = length;
    }
    public int getLength() {
        return length;
    }
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length);
        if(getFILL()==true) g.fillRect(getX(),getY(),length,length);
    }
}
