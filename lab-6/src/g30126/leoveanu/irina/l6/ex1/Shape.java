package g30126.leoveanu.irina.l6.ex1;
import java.awt.*;

public abstract class Shape {

    private Color color;
    private int x;
    private int y;
    final private String id;
    private boolean fill;

    public Shape(Color color, String id, int x, int y, boolean fill) {
        this.color = color;
        this.id = id;
        this.x = x;
        this.y = y;
        this.fill = fill;
    }

    public Color getColor() {
        return color;
    }
    public int getX(){return x;}
    public int getY(){return y;}
    public boolean getFILL() {return fill;}
    public String getId(){return id;}
    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}
