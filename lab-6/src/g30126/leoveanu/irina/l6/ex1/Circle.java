package g30126.leoveanu.irina.l6.ex1;
import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int radius, String id, int x, int y, boolean fill) {
        super(color,id,x,y,fill);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(getFILL()==true) g.fillOval(getX(),getY(),radius,radius);
    }
}