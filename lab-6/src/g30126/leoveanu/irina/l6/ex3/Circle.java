package g30126.leoveanu.irina.l6.ex3;
import java.awt.*;
public class Circle implements Shape {
    private Color color;
    private int x;
    private int y;
    final private String id;
    private boolean fill;
    private int radius;

    public Circle(Color color, int radius, String id, int x, int y, boolean fill) {
        this.color = color;
        this.id = id;
        this.x = x;
        this.y = y;
        this.fill = fill;
        this.radius = radius;
    }
    public Color getColor() {
        return color;
    }
    public int getX(){return x;}
    public int getY(){return y;}
    public boolean getFILL() {return fill;}
    public String getId(){return id;}
    public void setColor(Color color) {
        this.color = color;
    }

    public int getRadius() {
        return radius;
    }
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(getFILL()==true) g.fillOval(getX(),getY(),radius,radius);
    }
}
