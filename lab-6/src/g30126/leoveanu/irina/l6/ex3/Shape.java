package g30126.leoveanu.irina.l6.ex3;

import java.awt.*;

public interface Shape {
    String getId();
     void draw(Graphics g);
}
