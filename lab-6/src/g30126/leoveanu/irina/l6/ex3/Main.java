package g30126.leoveanu.irina.l6.ex3;
import java.awt.*;

public class Main {
    public static void main(String[] args)// throws InterruptedException {
    { DrawingBoard b1 = new DrawingBoard();
        Shape[] s = new Shape[5];
        s[0]= new Circle(Color.RED, 90, "Figura 1 cerc",50,50,true);
        b1.addShape(s[0]);
        s[1] = new Circle(Color.GREEN, 100, "Figura 2 cerc",50,50,false);
        b1.addShape(s[1]);
        s[2] = new Rectangle(Color.BLACK, 100, "Figura 3 patrat",100,150,true);
        b1.addShape(s[2]);

        //Thread.sleep(1000);
        b1.deleteByld("Figura 3 patrat");
    }
}
