package g30126.leoveanu.irina.l6.ex3;

import java.awt.*;

public class Rectangle implements Shape {
    private Color color;
    private int x;
    private int y;
    final private String id;
    private boolean fill;
    private int length;

    public Rectangle(Color color, int length, String id, int x, int y, boolean fill) {
        this.color = color;
        this.id = id;
        this.x = x;
        this.y = y;
        this.fill = fill;
        this.length = length;
    }
    public Color getColor() {
        return color;
    }
    public int getX(){return x;}
    public int getY(){return y;}
    public boolean getFILL() {return fill;}
    public String getId(){return id;}
    public void setColor(Color color) {
        this.color = color;
    }
    public int getLength() {
        return length;
    }
    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length);
        if(getFILL()==true) g.fillRect(getX(),getY(),length,length);
    }
}
