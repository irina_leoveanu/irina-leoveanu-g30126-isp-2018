package g30126.leoveanu.irina.l6.ex5;

import javax.swing.*;
import java.awt.*;
import java.lang.String;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    private ArrayList<Shape> shapes;

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
        shapes = new ArrayList<Shape>();
    }

    public void addShape(Shape s1){
        for(int i=0;i<shapes.size();i++){
            if(shapes.get(i)==null){
                shapes.set(i,s1);
                break;
            }
        }
//        shapes.add(s1);
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.size();i++)
            if (shapes.get(i) == null)
                shapes.get(i).draw(g);
    }

    public void deleteByld(String id)
    {
        for(int i=0;i<shapes.size();i++)
            if ((shapes.get(i) != null) && shapes.get(i).getId().equals(id))
            {
                System.out.println("Deleting a shape at coordinates " + shapes.get(i).getX() + " " + shapes.get(i).getY());
                System.out.println();
                shapes.set(i,null);
            }
        // repaint();
    }
}
