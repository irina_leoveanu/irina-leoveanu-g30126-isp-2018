package g30126.leoveanu.irina.l6.ex4;
import java.lang.CharSequence;

public class Clasuta implements CharSequence {
    private String s;

    public Clasuta(String s) {
        this.s = s;
    }

    @Override
    public char charAt(int i) {
        if ((i < 0) || (i >= s.length())) {
            throw new StringIndexOutOfBoundsException(i);
        }
        return s.charAt(i);
    }
    @Override
    public int length() {
        return s.length();
    }
    @Override
    public CharSequence subSequence(int start, int end) {
        if (start < 0) {
            throw new StringIndexOutOfBoundsException(start);
        }
        if (end > s.length()) {
            throw new StringIndexOutOfBoundsException(end);
        }
        if (start > end) {
            throw new StringIndexOutOfBoundsException(start - end);
        }
        StringBuilder sub = new StringBuilder(s.subSequence(start,end));
        return sub;
    }
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder(this.s);
        return s.toString();
    }

}