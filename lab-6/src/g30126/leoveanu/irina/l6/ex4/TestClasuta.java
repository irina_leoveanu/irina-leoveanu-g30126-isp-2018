package g30126.leoveanu.irina.l6.ex4;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestClasuta {
    @Test
    public void shouldReturnCorrectForm(){
        Clasuta s = new Clasuta("String to test the methods");
       assertEquals("String to test the methods", s.toString());
    }
    @Test
    public void shouldReturnCorrectLetters(){
        Clasuta s = new Clasuta("String to test the methods");
        assertEquals('g', s.charAt(5));
        assertEquals('t', s.charAt(10));
    }
    @Test
    public void shouldReturnCorrectSubString(){
        Clasuta s = new Clasuta("String to test the methods");
        assertEquals("g to ", s.subSequence(5,10).toString());

    }
}
