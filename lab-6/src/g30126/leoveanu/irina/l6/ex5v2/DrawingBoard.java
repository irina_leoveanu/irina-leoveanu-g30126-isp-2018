package g30126.leoveanu.irina.l6.ex5v2;

import javax.swing.*;
import java.awt.*;
import java.lang.String;

public class DrawingBoard  extends JFrame {

    Shape[] shapes = new Shape[10];

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                break;
            }
        }
//        shapes.add(s1);
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
//        for(Shape s:shapes)
//            s.draw(g);
    }

    public void deleteByld(String id)
    {
        for(int i=0;i< shapes.length;i++)
            if ((shapes[i] != null) && shapes[i].getId().equals(id))
            {
                System.out.println("Deleting a shape at coordinates " + shapes[i].getX() + " " + shapes[i].getY());
                System.out.println();
                shapes[i] = null;
            }
        // repaint();
    }
}
