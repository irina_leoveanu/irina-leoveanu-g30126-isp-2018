package g30126.leoveanu.irina.l6.ex5v2;

public class Pyramid {
    private int bricks;

    public Pyramid(int bricks) {
        this.bricks = bricks;
    }

    public boolean isFULL()
    {
        int i=0;
        int s=0;
        while(s<bricks) {
            s = s + i;
            i++;
        }
        if(bricks==s) return true;
        return false;
    }
    public int getBase()
    {

        int i=0;
        int s=0;
        while(s<bricks) {
            s = s + i;
            i++;
        }
        return i-1;
    }
    public int getExtra()
    {
        int i=0;
        int s=0;
        for(i=0;i<getBase();i++) {
            s = s + i;
        }
        return bricks-s;
    }

}
