package g30126.leoveanu.irina.l6.ex5v2;
import java.awt.*;
import java.util.Scanner;

public class DrawPyramid {
    public static void main(String[] args)
    {
        System.out.println("Enter the numbers of bricks(max 10)");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println("Enter the dimensions of one brick");
        int len = in.nextInt();
        int wid = in.nextInt();
        Pyramid pyr_init = new Pyramid(n);
        Shape[] s = new Shape[n+1];
        int base = pyr_init.getBase();
        boolean f = pyr_init.isFULL();
        int extra = 0;
        if (f == false)
            extra = pyr_init.getExtra();
        System.out.println("bricks " + n + " base " + base + " full " + f + " extra bricks " + extra);
        int N = 0;
        //if (f == false)
        N = n - extra;
        Pyramid pyr_full = new Pyramid(N);
        int base_f = pyr_full.getBase();
        int bf = base_f;
        int o, k = 0;
        DrawingBoard b1 = new DrawingBoard();
        while (bf != 0) {
            for (o = 0; o < bf && k < n; o++) {
                s[k]=new Rectangle(Color.BLACK, len, wid, "Fig " + k, 10 + (base_f - bf) * len/2 + o * len, 200 - (base_f - bf) * wid, false);
                b1.addShape(s[k]);
                k++;
            }
            bf--;
        }
        int niv=0;
        while(extra!=0)
        {
            s[k] = new Rectangle(Color.BLACK, len, wid, "Fig " + k, 10 - niv * len/2 + base_f * len, 200 - niv * wid, true);
            b1.addShape(s[k]);
            k++;
            niv++;
            extra--;
        }


    }

}