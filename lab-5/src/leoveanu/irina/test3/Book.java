package leoveanu.irina.test3;

import java.util.Random;

public class Book {

    private int x;
    private int y;
    private String location;

    public Book(int x)
    {
        this.x=x;
        this.y=0;
        this.location="";
    }
    public String read()
    {
        return "Sensor value is "+x;
    }

    public static void main(String[] args)
    {
        Random random = new Random();
        Book[] array = new Book[4];
        array[0]= new Book(random.nextInt(10));
        array[1]= new Sensor(random.nextInt(10),random.nextInt(10));
        array[2]= new TemperatureSensor(random.nextInt(10),random.nextInt(10));
        array[3]= new HumiditySensor(random.nextInt(10),random.nextInt(10));
        array[0].read();
        array[1].read();
        array[2].read();
        array[3].read();

    }
}
