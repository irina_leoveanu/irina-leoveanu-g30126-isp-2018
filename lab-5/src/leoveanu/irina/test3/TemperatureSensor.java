package leoveanu.irina.test3;

public class TemperatureSensor extends Book {
    private int y;

    public TemperatureSensor(int x, int y)
    {
        super(x);
        this.y=y;
    }
    @Override
    public String read()
    {
        return "TemperatureSensor value is "+this.y;
    }
}
