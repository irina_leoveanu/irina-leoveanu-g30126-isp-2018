package leoveanu.irina.test3;

public class HumiditySensor extends Book{
    private int y;

    public HumiditySensor(int x, int y)
    {
        super(x);
        this.y=y;
    }
    @Override
    public String read()
    {
        return "HumiditySensor value is "+this.y;
    }
}
