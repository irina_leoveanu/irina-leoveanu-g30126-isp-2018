package g30126.leoveanu.irina.l5.ex4;

public class TestSensor {
    public static void main(String[] args) throws InterruptedException {
        Controller c = Controller.getInstance();
        c.control();
        TemperatureSensor t = TemperatureSensor.getInstance();
        System.out.println(t.getLocation());
    }
}
