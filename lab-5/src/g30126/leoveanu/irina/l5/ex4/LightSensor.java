package g30126.leoveanu.irina.l5.ex4;

import java.util.Random;

public class LightSensor extends Sensor {
    private static LightSensor lightSensor = new LightSensor();
    private LightSensor() {}
    public static LightSensor getInstance() {
        synchronized (LightSensor.class) {
            if (lightSensor == null)
                lightSensor = new LightSensor();
        }
        return lightSensor;
    }

    public int readValue()
    {
        Random random = new Random();
        return random.nextInt(100);
    }
}
