package g30126.leoveanu.irina.l5.ex4;

import java.util.Random;

public class TemperatureSensor extends Sensor {
    private static TemperatureSensor tempSensor = new TemperatureSensor();
    private TemperatureSensor() {}
    public static TemperatureSensor getInstance() {
        synchronized (TemperatureSensor.class) {
            if (tempSensor == null)
                tempSensor = new TemperatureSensor();
        }
        return tempSensor;
    }
    public int readValue()
    {
        Random random = new Random();
        return random.nextInt(100);
    }
}
