package g30126.leoveanu.irina.l5.ex4;

public class Controller {
    private static Controller controller = new Controller();
    private static TemperatureSensor tempSensor = TemperatureSensor.getInstance();
    private static LightSensor lightSensor = LightSensor.getInstance();

    private Controller() {}
    public static Controller getInstance() {
        synchronized (Controller.class) {
            if (controller.tempSensor == null)
                tempSensor = TemperatureSensor.getInstance();
            if (controller.lightSensor == null)
                lightSensor = LightSensor.getInstance();
        }
        return controller;
    }

    public void control() throws InterruptedException {
        int x;
        int y;
        for (int i = 0;i < 20;i++) {
            x = tempSensor.readValue();
            y = lightSensor.readValue();

            String sensorsInfo[] = {
                    "Temperature sensor value is "+x,
                    "Light sensor value is "+y};
            Thread.sleep(1000);
            //Print a message
            System.out.println(sensorsInfo[0]+" "+sensorsInfo[1]);
        }
    }
}

