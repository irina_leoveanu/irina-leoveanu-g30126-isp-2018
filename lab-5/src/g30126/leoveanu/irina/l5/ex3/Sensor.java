package g30126.leoveanu.irina.l5.ex3;

public abstract class Sensor {

    private String location;

    abstract public int readValue();

    public String getLocation()
    {
        return location;
    }
}
