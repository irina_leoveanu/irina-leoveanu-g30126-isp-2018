package g30126.leoveanu.irina.l5.ex3;

public class Controller {
    public TemperatureSensor tempSensor;
    public LightSensor lightSensor;

    public Controller()
    {
        tempSensor = new TemperatureSensor();
        lightSensor = new LightSensor();
    }
    public void control() throws InterruptedException {
        int x;
        int y;
         for (int i = 0;i < 20;i++) {
             x = tempSensor.readValue();
             y = lightSensor.readValue();

             String importantInfo[] = {
                     "Temperature sensor value is "+x,
                     "Light sensor value is "+y};
             Thread.sleep(1000);
            //Print a message
            System.out.println(importantInfo[0]+" "+importantInfo[1]);
        }
        }
    }

