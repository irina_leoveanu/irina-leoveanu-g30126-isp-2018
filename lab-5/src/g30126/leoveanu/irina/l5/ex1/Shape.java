package g30126.leoveanu.irina.l5.ex1;

public abstract class Shape {
    protected  String color;
    protected boolean filled;

    public Shape()
    {
        color="green";
        filled=true;
    }
    public Shape(String color, boolean filled)
    {
        this.color=color;
        this.filled=filled;
    }
    public String getColor()
    {
        return this.color;
    }
    public void setColor(String color)
    {
        this.color=color;
    }
    public boolean isFILLED()
    {
        return filled;
    }
    public void setFilled(boolean filled)
    {
        this.filled=filled;
    }

    public String toString()
    {
        if(filled==true)
            return "A shape with color of "+color+" and filled";
        else
            return "A shape with color of "+color+" and Not filled";
    }
    abstract public double getArea();
    abstract public double getPerimeter();

    public static void main(String[] args)
    {
        Shape[] a1 = new Shape[3];
        a1[0]= new Circle();
        a1[1]= new Rectangle();
        a1[2]= new Square();
        System.out.println("aria "+a1[0].getArea());
        System.out.println("perimetru "+a1[0].getPerimeter());
        System.out.println("aria "+a1[1].getArea());
        System.out.println("perimetru "+a1[1].getPerimeter());
        System.out.println("aria "+a1[2].getArea());
        System.out.println("perimetru "+a1[2].getPerimeter());

    }
}
