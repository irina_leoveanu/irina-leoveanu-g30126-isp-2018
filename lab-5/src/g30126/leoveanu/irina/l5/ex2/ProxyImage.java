package g30126.leoveanu.irina.l5.ex2;

public class ProxyImage implements Image {

    private Image image;
    private String fileName;
    private int type;

    public ProxyImage(String fileName, int type) {
        this.fileName = fileName;
        this.type = type;
    }

    public int getType() {
        return type;
    }

    @Override
    public void display() {

        if (image == null && this.getType() == 1) {
            image = new RealImage(fileName);
        }
        if (image == null && this.getType() == 0) {
            image = new RotatedImage(fileName);
        }
        image.display();

    }
}
