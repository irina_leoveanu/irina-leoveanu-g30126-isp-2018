package g30126.leoveanu.irina.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class Ex7 {
	static int chance(int a, int b)
	{
		if(a<b) 
		{
			System.out.println("Wrong answer, your number it too high");
			return 0;
		}
		else if(a>b) 
		{
			System.out.println("Wrong answer, your number is too low");
			return 0;
		}
		else 
		{
			System.out.println("Correct answer");
			return 0;
		}
		
	}
	public static void main(String[] args)
	{
		Random r = new Random();
		int c=1;
		Scanner in = new Scanner(System.in);
		int a=0;
		int x= r.nextInt();
		while(c<4 && a==0)
		{
			System.out.println("Chance no"+c+": Please enter your guess");
		      int X = in.nextInt();
			a=chance(x,X);
			c++;
		}
		if(a==0) 
			System.out.println("You lost");
		else 
			System.out.println("You won");
		in.close();
	}
	
}
