package g30126.leoveanu.irina.l2.e6;

import java.util.Scanner;

public class Ex6 {

	static void m1(int a)
	{
		if(a==1 || a==0) System.out.println(" non recursive method "+a+"! = 1");
		else 
		{
			int p=1;
			int i;
			for(i=1;i<=a;i++)
				p=p*i;
			System.out.println("the non recursive method "+a+"! = "+p);
		}
	}
	static int m2(int a)
	{
		if(a==1) return 1;
		else if(a==0) return 1;
		else return a*m2(a-1);
		
	}
	public static void main(String[] args)
	{
		System.out.println(" Enter the number N" );
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		m1(N);
    	int p = m2(N);
    	System.out.println("the recursive method "+N+"! = "+p);
    	in.close();
}
}