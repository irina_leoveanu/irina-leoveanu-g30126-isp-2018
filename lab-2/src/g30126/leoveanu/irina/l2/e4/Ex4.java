package g30126.leoveanu.irina.l2.e4;

import java.util.Random;
import java.util.Scanner;

public class Ex4 {

	static void random_gen_vect(int x)
	{
		int i;
		int[] v;
		v = new int[x];
		Random r = new Random();
		for(i=0;i<x;i++)
			v[i] = r.nextInt();
		int max=v[0];
		for(i=1;i<x;i++)
			if(max<v[i])
				max=v[i];
			System.out.println("the max of random numbers is "+max);
	}
	static void vect(int x)
	{
		int i;
		int[] v;
		v=new int[x];
		System.out.println("The "+x+" numbers are ");
		Scanner in = new Scanner(System.in);
		for(i=0;i<x;i++)
			v[i] = in.nextInt();
		int max=v[0];
		for(i=1;i<x;i++)
			if(max<v[i])
				max=v[i];
		System.out.println("the max of given numbers is "+max);
		in.close();
	}
public static void main(String[] args)
{	
	System.out.println("Please enter the number N of elements");
	Scanner in = new Scanner(System.in);
    int N = in.nextInt();
    random_gen_vect(N);
    vect(N);
    in.close();
}
}
