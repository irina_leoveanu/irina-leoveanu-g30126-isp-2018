package g30126.leoveanu.irina.l2.e3;

import java.util.Scanner;

public class Ex3 {
	static int prim(int x)
	{
		int i;
		for(i=2;i<=x/2;i++) 
			if(x%i==0)
				return 0;
		return 1;
	}

	static int interval(int a, int b)
	{int i;
	int k=0;
	for(i=a;i<=b;i++)
		if(prim(i)==1) 
		{
			System.out.println(+i+" ");
			
			k++;
		}
	return k;
	}
	public static void main(String[] args)
	{
		 System.out.println("Please enter 2 numbers A and B");
		 Scanner in = new Scanner(System.in);
	     int A = in.nextInt();
	     int B = in.nextInt();
	     if (A>B) 
	     {
	    	 int C=A;
	    	 A=B;
	    	 B=C;
	     }
	     int n = interval(A,B);
	     System.out.println("Between "+A+" and "+B+" are "+n+" numbers");
	     in.close();
	}
	}
