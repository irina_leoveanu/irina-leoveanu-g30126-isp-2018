package g30126.leoveanu.irina.e15;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class RoboMarket {
	public static void main(String[] args)
	   {
	      City LA = new City(); 
	      Wall blockAve0 = new Wall(LA, 2, 3, Direction.WEST);
	      Wall blockAve1 = new Wall(LA, 2, 3, Direction.NORTH);
	      Wall blockAve2 = new Wall(LA, 2, 3, Direction.EAST);
	      Wall blockAve3 = new Wall(LA, 3, 3, Direction.EAST);
	      Wall blockAve4 = new Wall(LA, 3, 3, Direction.SOUTH);
	      Thing parcel1 = new Thing(LA, 0, 0); 
	      Thing parcel2 = new Thing(LA, 1, 0);
	      Thing parcel3 = new Thing(LA, 1, 1);
	      Thing parcel4 = new Thing(LA, 1, 2);
	      Thing parcel5 = new Thing(LA, 2, 2);
	      Robot Karel = new Robot(LA, 3,3, Direction.EAST);  
	      Robot Maria = new Robot(LA, 0,1, Direction.WEST); 
	      Maria.setLabel("M");
	      Karel.setLabel("K");
	      Maria.move();
	      Maria.pickThing();
	      Karel.turnLeft();
	      Karel.turnLeft();
	      Karel.move();
	      Maria.turnLeft();
	      Maria.move();
	      Karel.turnLeft();
	      Karel.turnLeft();
	      Karel.turnLeft();
	      Karel.move();
	      Karel.pickThing();
	      Maria.pickThing();
	      Maria.turnLeft();
	      Maria.move();
	      Karel.move();
	      Karel.turnLeft();
	      Maria.pickThing();
	      Karel.pickThing();
	   }
}