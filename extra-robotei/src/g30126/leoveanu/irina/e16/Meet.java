package g30126.leoveanu.irina.e16;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class Meet {
	public static void main(String[] args)
	   {
	      City LA = new City(); 
	      Wall blockAve0 = new Wall(LA, 0, 1, Direction.WEST);
	      Wall blockAve1 = new Wall(LA, 1, 1, Direction.WEST);
	      Wall blockAve2 = new Wall(LA, 1, 1, Direction.SOUTH);
	      Robot Karel = new Robot(LA, 0, 0, Direction.SOUTH);  
	      Robot Maria = new Robot(LA, 0, 1, Direction.SOUTH); 
	      Maria.move();
	      Karel.move();
	      Maria.turnLeft();
	      Maria.move();
	      Karel.move();
	      Maria.turnLeft();
	      Maria.turnLeft();
	      Maria.turnLeft();
	      Maria.move();
	      Karel.turnLeft();
	      Karel.move();
	      Maria.turnLeft();
	      Maria.turnLeft();
	      Maria.turnLeft();
	      Maria.move();
	      
	   }
}
