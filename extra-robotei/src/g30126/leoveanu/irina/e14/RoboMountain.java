package g30126.leoveanu.irina.e14;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;

public class RoboMountain {
	public static void main(String[] args)
	   {
	      City LA = new City(); 
	    //construct the mountain
	      Wall blockAve0 = new Wall(LA, 3, 2, Direction.WEST);
	      Wall blockAve1 = new Wall(LA, 3, 2, Direction.NORTH);
	      Wall blockAve2 = new Wall(LA, 2, 3, Direction.WEST);
	      Wall blockAve3 = new Wall(LA, 1, 3, Direction.WEST);
	      Wall blockAve4 = new Wall(LA, 1, 3, Direction.NORTH);
	      Wall blockAve5 = new Wall(LA, 1, 3, Direction.EAST);
	      Wall blockAve6 = new Wall(LA, 2, 4, Direction.NORTH);
	      Wall blockAve7 = new Wall(LA, 2, 4, Direction.EAST);
	      Wall blockAve8 = new Wall(LA, 3, 4, Direction.EAST);
	      //initial place of the flag
	      Thing parcel = new Thing(LA, 3, 1); 
	      //initial place of robot-Iribotel ^_^
	      Robot iribotel = new Robot(LA, 3,0, Direction.EAST);     
	      iribotel.move();
	      //pick up the flag
	      iribotel.pickThing();
	      //climb the mountain
	      iribotel.turnLeft();
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.turnLeft();
	      iribotel.turnLeft();
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.move();
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.turnLeft();
	      iribotel.turnLeft();
	      iribotel.move(); 
	      //put the flag on top of mountain
	      iribotel.putThing();
	     //get down of the mountain
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.turnLeft();
	      iribotel.turnLeft();
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.move();
	      iribotel.turnLeft();
	      iribotel.turnLeft();
	      iribotel.turnLeft();
	      iribotel.move();
	      iribotel.move();
	      iribotel.turnLeft();
	      //back at the base of the mountain, safe and sound!
}
}
