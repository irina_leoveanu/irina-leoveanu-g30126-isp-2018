package g30126.leoveanu.irina.l4.ex3;

public class Circle {
	private double radius;
	private String color;
	public Circle()
	{
	radius=1;
	color = "red";
	}

	public Circle(double radius)
	{
		this.radius=radius;
		this.color = "red";
	}
	
	public double getRadius(){
		return radius;
	}
	
	public double getArea()
	{
		return 3.1415*radius*radius;
	}
	
	public static void main(String[] args)
	{
		Circle c1 = new Circle();
		double x=c1.getArea();
		System.out.println(c1.radius+c1.color+x);
		Circle c2 = new Circle(7);
		double y=c2.getArea();
		System.out.println(c2.radius+c2.color+y);
		
	}
}
