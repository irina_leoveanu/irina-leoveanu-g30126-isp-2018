package g30126.leoveanu.irina.l4.ex4;

public class Author {

	private String name;
	private String email;
	private char gender;
	public Author(String name,String email, char gender)
	{
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	 public String getName()
	 {
		 return this.name;
	 }
	 
	 public String getEmail()
	 { 
		 return this.email;
	 }
	 
	 public void setEmail(String email)
	 {
		 this.email=email;
	 }
	 
	 public char getGender()
	 {
		 return this.gender;
	 }
	 
	 public String toString()
	 {
		 return "Author-"+name+"("+gender+")"+" at "+email;
	 }
	 
	 public static void main(String[] args)
	 {
		 Author a=new Author("Cassandra Clare","cassy.clare",'f');
		 String b;
		 b=a.toString();
		 System.out.println(b);
		 a.setEmail("cassie.clare");
		 b=a.toString();
		 System.out.println(b);
	 }
}
