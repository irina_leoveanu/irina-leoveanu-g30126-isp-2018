package g30126.leoveanu.irina.l4.ex4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAuthor {

	@Test
	public void shouldReturnCorrectForm(){
		Author a=new Author("CassandraClare","cassy.clare",'f');
       
		 assertEquals("Author-CassandraClare(f) at cassy.clare", a.toString());
		    }
}
