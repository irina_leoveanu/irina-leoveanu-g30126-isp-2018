package g30126.leoveanu.irina.l4.ex8v2;

public class Shape {

	private String color;
	private boolean filled;
	
	public Shape()
	{
		color="green";
		filled=true;
	}
	public Shape(String color, boolean filled)
	{
		this.color=color;
		this.filled=filled;
	}
	public String getColor()
	{
		return this.color;
	}
	public void setColor(String color)
	{
		this.color=color;
	}
	public boolean isFILLED()
	{
		return filled;
	}
	public void setFilled(boolean filled)
	{
		this.filled=filled;
	}

	public String toString()
	{
		if(filled==true)
			return "A shape with color of "+color+" and filled";
		else
			return "A shape with color of "+color+" and Not filled";
	}

}
