package g30126.leoveanu.irina.l4.ex8v2;

public class Testare {

	public static void main(String[] args)
	{
		//test square methods
		Square ob1 = new Square();
		System.out.println(ob1.toString());
		ob1=new Square(2);
		System.out.println("side "+ob1.getSide()+" color "+ob1.getColor()+" area "+ob1.getArea()+" perimeter "+ob1.getPerimeter()+" is it filled: "+ob1.isFILLED());
		ob1.setColor("brown");
		ob1.setFilled(false);
		ob1.setLength(4);
		System.out.println(ob1.toString());
		System.out.println();
		
		//test circle methods
		Circle ob2 = new Circle();
		Circle ob3 = new Circle(2);
		Circle ob4 = new Circle(3,"blue",false);
		System.out.println(ob2.toString());
		ob2.setFilled(false);
		ob2.setColor("orange");
		System.out.println(ob2.toString());
		System.out.println(ob3.getArea()+" cm^2 "+ob3.getPerimeter()+" cm "+ob3.getRadius()+" Cm ");
		System.out.println(ob4.toString());
		System.out.println();
		
		//test shape methods
		Shape ob5 = new Shape();
		System.out.println(ob5.toString());
		ob5 = new Shape("yellow",false);
		System.out.println(ob5.getColor()+" "+ob5.isFILLED());
		ob5.setFilled(true);
		System.out.println(ob5.toString());
		System.out.println();
		
		//test rectangle methods
		Rectangle ob6 = new Rectangle();
		System.out.println(ob6.toString());
		ob6 = new Rectangle(2.3,4.5);
		System.out.println("length "+ob6.getLength()+" width "+ob6.getWidth()+" color "+ob6.getColor()+" area "+ob6.getArea()+" perimeter "+ob6.getPerimeter());
		ob6 = new Rectangle(2.3,4.5,"white",false);
		ob6.setColor("black");
		ob6.setFilled(true);
		ob6.setLength(3.14);
		ob6.setWidth(0.5);
		System.out.println("length "+ob6.getLength()+" width "+ob6.getWidth()+" color "+ob6.getColor()+" area "+ob6.getArea()+" perimeter "+ob6.getPerimeter());
		System.out.println();
}
}