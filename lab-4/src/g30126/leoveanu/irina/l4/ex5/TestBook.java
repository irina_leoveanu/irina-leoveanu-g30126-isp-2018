package g30126.leoveanu.irina.l4.ex5;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import g30126.leoveanu.irina.l4.ex4.Author;

public class TestBook {

	@Test
	public void shouldReturnCorrectForm(){
		Author a1 = new Author("Cassandra Clare","cassy.clare@gmail.com",'f');
		Book b1 = new Book("The Bane Chronicles",a1,12.9,6);
		 assertEquals("book-'The Bane Chronicles'by Author-Cassandra Clare(f) at cassy.clare@gmail.com", b1.toString());
		    }
}
