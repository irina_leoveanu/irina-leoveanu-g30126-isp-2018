package g30126.leoveanu.irina.l4.ex5;
import g30126.leoveanu.irina.l4.ex4.*;;
public class Book {	
	private String name;
	private Author author;
	private double price;
	private int qtyInStock=0;
	
	//constructor
	public Book(String name, Author author,double price)
	{
		this.name=name;
		this.author=author;
		this.price=price;
	}
	public Book(String name, Author author,double price, int qtyInStock)
	{
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	public String getName()
	{
		return name;
	}
	public Author getAuthor()
	{
		return author;
	}
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price=price;
	}
	public int getQtyInStock()
	{
		return qtyInStock;
	}
	public void setQtyInStock(int qtyInStock)
	{
		this.qtyInStock=qtyInStock;
	}
	public String toString()
	{
		return "book-'"+this.name+"'by "+author.toString();
	}
	public static void main(String[] args)
	{
		Author a1 = new Author("Cassandra Clare","cassy.clare@gmail.com",'f');
		Book b1 = new Book("City of bones",a1,12.9,6);
		System.out.println(b1.toString());
		Book b2 = new Book("City of ashes",a1,12.9);
		b2.setQtyInStock(20);
		System.out.println(b2.getName()+" by "+b2.author.getName()+" at price of "+b2.getPrice()+" We have "+b2.getQtyInStock());
		b2.setPrice(b2.getPrice()*0.7);
		System.out.println("ON SALE NOW! "+b2.getName()+" by "+b2.author.getName()+" at price of "+b2.getPrice()+" We have "+b2.getQtyInStock());
		b2.setQtyInStock(0);
		System.out.println("Sorry, we ran out of "+b2.getName()+" by "+b2.author.getName()+" at price of "+b2.getPrice()+" We have "+b2.getQtyInStock());
		
		
	}
}
