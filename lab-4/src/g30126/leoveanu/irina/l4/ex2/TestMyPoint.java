package g30126.leoveanu.irina.l4.ex2;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestMyPoint {
	
	@Test
    public void shouldBeCorrect(){
		MyPoint A = new MyPoint();
		 assertEquals("(0,0)", A.toString());
	}
	@Test
	public void shouldBeEqual()
	{
		MyPoint A = new MyPoint(2,3);
		assertEquals(2,A.getX());
	}
	@Test
	public void shouldBeDistance()
	{
		MyPoint A = new MyPoint(2,3);
		MyPoint B = new MyPoint(2,1);
		assertEquals(2,A.distance(B),0.01);
	}
}
