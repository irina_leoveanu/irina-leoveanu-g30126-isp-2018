package g30126.leoveanu.irina.l4.ex7;
import g30126.leoveanu.irina.l4.ex3.*;

public class Cylinder extends Circle {

	private double height;
	public Cylinder()
	{
		super();
		height=1;
	}
	public Cylinder(double radius)
	{
		super(radius);
		this.height=1;
	}
	public Cylinder(double radius,double height)
	{
		super(radius);
		this.height=height;
	}
	@Override 
	public double getArea()
	{
		return 2*3.1415*getRadius()*getHeight()+2*3.1415*getRadius()*getRadius();
	}
	public double getHeight()
	{
		return height;
	}
	public double getVolume()
	{
		return getRadius()*getRadius()*3.1415*height;
	}
	public static void main(String[] args)
	{
		Cylinder c1 = new Cylinder();
		System.out.println("inaltimea "+c1.getHeight()+" raza "+c1.getRadius()+" volum "+c1.getVolume());
		Cylinder c2 = new Cylinder(4.3);
		System.out.println("inaltimea "+c2.getHeight()+" raza "+c2.getRadius()+" volum "+c2.getVolume());
		Cylinder c3 = new Cylinder(2);
		System.out.println("inaltimea "+c3.getHeight()+" raza "+c3.getRadius()+" volum "+c3.getVolume());	
	}
}
