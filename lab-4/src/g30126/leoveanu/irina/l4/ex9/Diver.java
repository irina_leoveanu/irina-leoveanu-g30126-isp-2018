package g30126.leoveanu.irina.l4.ex9;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class Diver {
	private Robot robot;
	
	public Diver(City city, int a, int b, int c)
	{
		if(c==0)
		robot= new Robot(city,a,b,Direction.NORTH);
		else if(c==1)
			robot= new Robot(city,a,b,Direction.EAST);
		else if(c==2)
			robot= new Robot(city,a,b,Direction.SOUTH);
		else if(c==3)
			robot= new Robot(city,a,b,Direction.WEST);
	}
	public void dive()
	{
		robot.move();
		//turns right
		robot.turnLeft();
		robot.turnLeft();
		robot.turnLeft();
		robot.move();
		//turns right
		robot.turnLeft();
		robot.turnLeft();
		robot.turnLeft();
		robot.move();
		//first front flip
		robot.turnLeft();
		robot.turnLeft();
		robot.turnLeft();
		robot.turnLeft();
		//second front flip
		robot.turnLeft();
		robot.turnLeft();
		robot.turnLeft();
		robot.turnLeft();
		//third front flip
		robot.turnLeft();
		robot.turnLeft();
		robot.turnLeft();
		robot.turnLeft();
		robot.move();
		robot.move();
		robot.move();
		robot.turnLeft();
		robot.turnLeft();
	}
	public static void main(String[] args)
	{
		 City pool = new City(); 
	     Diver iribotel = new Diver(pool, 1,1, 0); 
	     Wall blockAve1 = new Wall(pool, 2, 1, Direction.NORTH);
	     Wall blockAve2 = new Wall(pool, 2, 0, Direction.EAST);
	     Wall blockAve3 = new Wall(pool, 3, 0, Direction.EAST);
	     Wall blockAve4 = new Wall(pool, 4, 0, Direction.EAST);
	     Wall blockAve5 = new Wall(pool, 4, 1, Direction.WEST);
	     Wall blockAve6 = new Wall(pool, 4, 1, Direction.SOUTH);
	     Wall blockAve7 = new Wall(pool, 4, 2, Direction.SOUTH);
	     Wall blockAve8 = new Wall(pool, 4, 2, Direction.EAST);
	     iribotel.dive();
	}
}
