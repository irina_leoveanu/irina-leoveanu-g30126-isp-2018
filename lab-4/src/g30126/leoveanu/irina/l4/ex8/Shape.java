package g30126.leoveanu.irina.l4.ex8;

public class Shape {

	private String color;
	private boolean filled;
	
	public Shape()
	{
		color="green";
		filled=true;
	}
	public Shape(String color, boolean filled)
	{
		this.color=color;
		this.filled=filled;
	}
	public String getColor()
	{
		return this.color;
	}
	public void setColor(String color)
	{
		this.color=color;
	}
	public boolean isFILLED()
	{
		return filled;
	}
	public void setFilled(boolean filled)
	{
		this.filled=filled;
	}
	@Override 
	public String toString()
	{
		if(filled==true)
			return "A shape with color of "+color+" and filled";
		else
			return "A shape with color of "+color+" and Not filled";
	}
}

class Circle extends Shape {
	private double radius;
	
	public Circle()
	{
		super();
		radius=1;
	}
	public Circle(double radius)
	{
		super();
		this.radius=radius;
	}
	public Circle(double radius,String color, boolean filled)
	{
		super(color,filled);
		this.radius=radius;
	}
	public double getRadius(){
		return radius;
	}
	public void setRadius(double radius)
	{
		this.radius=radius;
	}
	public double getArea()
	{
		return 3.1415*radius*radius;
	}
	public double getPerimeter()
	{
		return 2*3.1415*radius;
	}
	@Override 
	public String toString()
	{
		return "A Circle with radius="+radius+", which is a subclass of "+super.toString();
	}
}
class Rectangle extends Shape {
	private double width;
	private double length;
	
	public Rectangle()
	{
		super();
		width=1;
		length=1;
	}
	public Rectangle(double width, double length)
	{
		super();
		this.width=width;
		this.length=length;
		
	}
	public Rectangle(double width, double length, String color, boolean filled)
	{
		super(color, filled);
		this.width=width;
		this.length=length;
	}
	public double getWidth()
	{
		return width;
	}
	public void setWidth(double width)
	{
		this.width=width;
	}
	public double getLength()
	{
		return length;
	}
	public void setLength(double length)
	{
		this.length=length;
	}
	public double getArea()
	{
		return width*length;
	}
	public double getPerimeter()
	{
		return 2*(width+length);
	}
	@Override 
	public String toString()
	{
		return "A Rectangle with width="+width+" and length="+length+", which is a subclass of "+super.toString();
	}
}
class Square extends Rectangle {

	public Square()
	{
		super();
	}
	public Square(double side)
	{
		super(side, side);
	}
	public Square(double side, String color, boolean filled)
	{
		super(side,side,color,filled);
	}
	public double getSide()
	{
		return this.getLength();
	}
	public void setSide(double side)
	{
		super.setLength(side);
		super.setWidth(side);
	}
	@Override
	public void setWidth(double side)
	{
		super.setLength(side);
		super.setWidth(side);
	}

	@Override
	public void setLength(double side)
	{
		super.setLength(side);
		super.setWidth(side);
	}
	@Override 
	public String toString()
	{
		return "A Square with side="+ this.getSide()+", which is a subclass of "+super.toString();
	}
}