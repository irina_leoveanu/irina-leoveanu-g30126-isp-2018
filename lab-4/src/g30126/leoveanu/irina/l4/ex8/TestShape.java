package g30126.leoveanu.irina.l4.ex8;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestShape {
	@Test
    public void shouldArea(){
		Circle c1 = new Circle();
		 assertEquals(3.1415, c1.getArea(),0.01);
		    }
	@Test
    public void shouldBeFilled(){
		Shape c1 = new Shape();
		 assertEquals(true, c1.isFILLED());
		    }
	@Test
    public void shouldBeCorrect(){
		Rectangle c1 = new Rectangle();
		c1.toString();
		 assertEquals("A Rectangle with width=1.0 and length=1.0, which is a subclass of A shape with color of green and filled", c1.toString());
		    }
	
	 public void shouldBeFine(){
			Square a = new Square();
			a.toString();
			 assertEquals("A Square with side=1.0, which is a subclass of A Rectangle with width=1.0 and length=1.0, which is a subclass of A shape with color of green and filled", a.toString());
			    }
}
