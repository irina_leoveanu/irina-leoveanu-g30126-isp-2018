package g30126.leoveanu.irina.l4.ex6;
import g30126.leoveanu.irina.l4.ex4.*;
public class Book {

	private String name;
	private Author[] authors;
	private double price;
	private int qtyInStock=0;
	
	//constructor
	public Book(String name, Author[] authors,double price)
	{
		this.name=name;
		this.authors=authors;
		this.price=price;
	}
	public Book(String name, Author[] authors,double price, int qtyInStock)
	{
		this.name=name;
		this.authors=authors;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	public String getName()
	{
		return name;
	}
	public Author[] getAuthors()
	{
		return authors;
	}
	public double getPrice()
	{
		return price;
	}
	public void setPrice(double price)
	{
		this.price=price;
	}
	public int getQtyInStock()
	{
		return qtyInStock;
	}
	public void setQtyInStock(int qtyInStock)
	{
		this.qtyInStock=qtyInStock;
	}
	public String toString()
	{
		return "book-'"+this.name+"'by "+authors.length+" authors";
	}
	public static void main(String[] args)
	{
		Author[] a1 = new Author[3];
		a1[0]= new Author("Cassandra Clare","cassy.clare@gmail.com",'f');
		a1[1]= new Author("Sarah Rees Brennan","sarah.rees@gmail.com",'f');
		a1[2]= new Author("Maureen Johnson","mar.johnson@yahoo.com",'f');
		Book b1 = new Book("The Bane Chronicles",a1,12.9,6);
		System.out.println(b1.toString());
		System.out.println("The authors are ");
		int i;
		for(i=0;i<a1.length;i++)
		System.out.println(a1[i].toString());
		System.out.println(b1.getName()+" at price of "+b1.getPrice()+" We have "+b1.getQtyInStock());
		b1.setPrice(b1.getPrice()*0.7);
		System.out.println("ON SALE NOW! "+b1.getName()+" at price of "+b1.getPrice()+" We have "+b1.getQtyInStock());
		b1.setQtyInStock(0);
		System.out.println("Sorry, we ran out of "+b1.getName()+" at price of "+b1.getPrice()+" We have "+b1.getQtyInStock());
			
	}

}
