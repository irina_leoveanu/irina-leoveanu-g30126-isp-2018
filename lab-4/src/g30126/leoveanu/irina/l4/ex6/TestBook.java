package g30126.leoveanu.irina.l4.ex6;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

import g30126.leoveanu.irina.l4.ex4.Author;

public class TestBook {

	@Test
	public void shouldReturnCorrectForm(){
		Author[] a1 = new Author[3];
		a1[0]= new Author("Cassandra Clare","cassy.clare@gmail.com",'f');
		a1[1]= new Author("Sarah Rees Brennan","sarah.rees@gmail.com",'f');
		a1[2]= new Author("Maureen Johnson","mar.johnson@yahoo.com",'f');
		Book b1 = new Book("The Bane Chronicles",a1,12.9,6);
		 assertEquals("book-'The Bane Chronicles'by 3 authors", b1.toString());
		    }
}
