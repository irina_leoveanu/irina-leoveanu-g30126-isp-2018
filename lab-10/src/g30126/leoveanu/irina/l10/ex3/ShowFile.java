package g30126.leoveanu.irina.l10.ex3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class ShowFile extends JFrame
{
    JLabel name;
    JTextField tName;
    JTextArea tArea;
    JButton bShow;

    ShowFile(){

        setTitle("Test showing a named file");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,450);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=100;int height = 20;

        name = new JLabel("Enter File Name ");
        name.setBounds(10, 50, width, height);

        tArea = new JTextArea();
        tArea.setBounds(10,120,200, 200);

        tName= new JTextField();
        tName.setBounds(100,50,width, height);

        bShow = new JButton("Show");
        bShow.setBounds(10,100,width, height);

        bShow.addActionListener(new TratareButonShow());

        add(name);add(tArea);add(tName);add(bShow);

    }

    public static void main(String args[])
    {
        new ShowFile();
    }

    class TratareButonShow implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            String filename = ShowFile.this.tName.getText();
            try {
                BufferedReader in = new BufferedReader(new FileReader(filename));
                String s;
                while ((s = in.readLine()) != null) {
                    ShowFile.this.tArea.append(s);
                    ShowFile.this.tArea.append("\n");
                }
                in.close();
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
                ShowFile.this.tArea.setText("Error 404, file not found");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}