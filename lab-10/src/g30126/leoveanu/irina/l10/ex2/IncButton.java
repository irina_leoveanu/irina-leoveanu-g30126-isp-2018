package g30126.leoveanu.irina.l10.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.*;

public class IncButton extends JFrame {


    JLabel add;
    JTextArea tArea;
    JButton bAdd;

    IncButton(){

        setTitle("Test Increasing Counter when Button pressed");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,450);
        setVisible(true);
    }


    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        add = new JLabel("Counter ");
        add.setBounds(10, 50, width, height);

        tArea = new JTextArea();
        tArea.setBounds(70,50,width, height);

        bAdd = new JButton("Add");
        bAdd.setBounds(10,150,width, height);

        bAdd.addActionListener(new TratareButonAdd());

        add(add);add(tArea);add(bAdd);

    }

    public static void main(String[] args) {
        new IncButton();
    }
    class TratareButonAdd implements ActionListener {
        private int count;
        public void actionPerformed(ActionEvent e) {
            count ++;

            IncButton.this.tArea.setText(""+count);


        }
    }
}